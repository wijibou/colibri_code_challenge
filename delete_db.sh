#!/usr/bin/bash

docker-compose down -v

# Get the most recent Docker image ID
IMAGE_ID=$(docker images --format "{{.ID}} {{.CreatedAt}}" | sort -r | head -n 1 | awk '{print $1}')

# Check if IMAGE_ID has a value, then remove the image
if [ ! -z "$IMAGE_ID" ]; then
    echo "Removing image with ID: $IMAGE_ID"
    docker rmi "$IMAGE_ID"
else
    echo "No image found to remove."
fi
