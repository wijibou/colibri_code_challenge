"""
This script processes wind turbine data, performs outlier imputation, identifies and handles missing values, computes summary statistics, and finds outliers.
It uses a series of functions imported from 'process_wind_turbine_data' and connects to a PostgreSQL database to persist results.
"""

from pyspark.sql import SparkSession, DataFrame
from pyspark.sql.utils import AnalysisException, IllegalArgumentException
from py4j.protocol import Py4JJavaError


from process_wind_turbine_data import ( 
create_spark_session, read_data, imputeOutliers, split_df_by_missing_values, summary_statistics, find_outliers,write_to_database

)


def main():

  """
  Main execution function:
  - Ingests wind turbine data from a CSV file.
  - Performs data cleaning and outlier imputation.
  - Splits data based on missing values.
  - Computes summary statistics for the clean dataset.
  - Identifies and handles outliers.
  - Connects to PostgreSQL for data persistence.
  """

  raw_data_path = "../data"
  timestamp_format = "dd-MM-yyyy HH:mm:ss"
  
  data_file = f"{raw_data_path}/data_group_*.csv"
  schema = "timestamp STRING, turbine_id INT, wind_speed DOUBLE, wind_direction INT, power_output DOUBLE"

    # Database connection properties
  db_properties = {
      "user": "colibri",
      "password": "Password123!",
      "driver": "org.postgresql.Driver"
  }

  url = "jdbc:postgresql://localhost:5432/colibri"

  try:
      
      #  load csv files into a data frame ready for cleansing
      spark_context = create_spark_session("Ingest Wind Turbine Data","../jars/postgresql-42.7.1.jar")
      df_data_group = read_data(spark_context, data_file, schema)
      print("No records loaded=",df_data_group.count())
      df_data_group.show()

      # Perform outlier imputation
      df_data_group_imputed = imputeOutliers(df_data_group, ['wind_speed', 'wind_direction', 'power_output'],1.05)
      # df_data_group_imputed.show()
      print("No records after outliers imputed=",df_data_group_imputed.count())
      df_data_group_imputed.show()

      # Remove records with missing keys(turbine_id, timestamp) and write cleaned data to database
      df_data_group_clean, df_data_group_removed = split_df_by_missing_values(df_data_group_imputed, ['turbine_id', 'timestamp'])
      print("No. Records with missing keys removed from data=",df_data_group_removed.count())      
      write_to_database(df_data_group_clean.orderBy(['timestamp','turbine_id'],ascending=True),url,"wind_turbine.data_group_clean","append",db_properties["user"],db_properties["password"])

      # compute summary statistics on power_output for each turbine for a given date period and write result to database
      df_summary = summary_statistics(df_data_group_clean, 'turbine_id', 'power_output', "01/03/2022 02:00:00", "03/03/2022 02:00:00")
      write_to_database(df_summary.orderBy(['turbine_id','from_date'],ascending=True),url,"wind_turbine.power_output_stats","append",db_properties["user"],db_properties["password"])
      df_summary.show()

      # # find outliers in power_output for a given time period and write result to a database 
      df_outliers = find_outliers(df_data_group_clean, df_summary.select(['turbine_id', 'mean_power_output', 'stdv_power_output']),
                            'turbine_id', 'power_output', 'mean_power_output', 'stdv_power_output',"01/03/2022 02:00:00", "03/03/2022 02:00:00")
      if df_outliers.count() > 0:    
        write_to_database(df_outliers.\
        select( ['turbine_id','timestamp','power_output', 'mean_power_output', 'stdv_power_output']).\
        orderBy(['timestamp','turbine_id'],ascending=True)
        ,url,"wind_turbine.power_output_outliers","append",db_properties["user"],db_properties["password"])

      print("No of outliers found=",df_outliers.count())

  except AnalysisException as e:
    print(f"Failed to analyze a SQL query plan: {e}")
  except IllegalArgumentException as e:
    print(f"Passed an illegal or inappropriate argument: {e}")
  except (IndexError) as e:
    print(f"IndexError: {e}")
  except (ValueError) as e:
    print(f"ValueError: {e}")
  except (Py4JJavaError) as e:
    print(f"JVM Error: {e}")
  except Exception as e:
    print(f"An unexpected error occurred: {e}")

    
if __name__ == "__main__":
  main()







