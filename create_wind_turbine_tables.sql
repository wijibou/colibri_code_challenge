-- Create the schema if it doesn't exist
CREATE SCHEMA IF NOT EXISTS wind_turbine;

-- Create the table within the wind_turbine schema
CREATE TABLE IF NOT EXISTS wind_turbine.data_group_clean (
    timestamp       TIMESTAMP      NOT NULL,
    turbine_id      INTEGER        NOT NULL,
    wind_speed      DECIMAL(5,2)   NOT NULL,
    wind_direction  INTEGER        NOT NULL,
    power_output    DECIMAL(5,2)   NOT NULL
    -- PRIMARY KEY (turbine_id, timestamp)
);

CREATE TABLE IF NOT EXISTS wind_turbine.power_output_stats (
    turbine_id INTEGER,
    min_power_output DECIMAL(5, 2),
    max_power_output DECIMAL(5, 2),
    mean_power_output DECIMAL(5, 2),
    stdv_power_output DECIMAL(5, 2),
    from_date TIMESTAMP,
    to_date TIMESTAMP
);

CREATE TABLE IF NOT EXISTS wind_turbine.power_output_outliers (
    timestamp TIMESTAMP,
    turbine_id INTEGER,
    power_output DECIMAL(5, 2),
    mean_power_output DECIMAL(5, 2),
    stdv_power_output DECIMAL(5, 2)
    -- PRIMARY KEY (turbine_id, timestamp)
);
