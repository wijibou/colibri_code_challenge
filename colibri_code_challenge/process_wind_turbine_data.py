"""
This module provides functions to process and analyze wind turbine data using PySpark and write results to a PostgreSQL database.
It includes functions for creating Spark sessions, reading data, imputing outliers, handling missing values, summarizing statistics,
identifying outliers, and writing data to databases.
"""


from pyspark.sql import SparkSession, DataFrame
from pyspark.sql.functions import (
    col, percentile_approx, to_timestamp, trim, lit, 
    min, max, mean, stddev, count, isnan, isnull, when
)
from pyspark.sql.types import StructType, StructField, IntegerType, StringType, FloatType, TimestampType

from sqlalchemy import create_engine
from pandas import ( DataFrame as pdf, to_datetime)

def create_spark_session(app_name: str,jar_path:str) -> SparkSession:
  """
  Creates and returns a SparkSession with the given application name and JDBC jar path.

  Parameters:
  app_name (str): The name for the Spark application.
  jar_path (str): Path to the JDBC jar file necessary for database connections.

  Returns:
  SparkSession: A SparkSession object.
  """
  try:
      spark = SparkSession.builder.appName(app_name).config("spark.jars", jar_path).getOrCreate()
      return spark
  except Exception as e:
      print(f"Failed to create Spark session: {e}")
      raise

def read_data(spark: SparkSession, file_path: str, schema: str):
  """
  Reads data from a CSV file into a DataFrame using the given SparkSession, file path and timestamp format.

  Parameters:
  spark (SparkSession): The SparkSession object.
  file_path (str): Path to the CSV file to be read.
  timestamp_format (str): The format of timestamps in the file.

  Returns:
  DataFrame: A DataFrame containing the data from the CSV file.
  """
  try:
      df = spark.read.csv(file_path, schema=schema, header=True)
      df = df.withColumn("timestamp", col("timestamp").cast(TimestampType()))
      return df
  except Exception as e:
      print(f"Failed to read data in {file_path}: {e}")
      raise

def imputeOutliers(input_df: DataFrame, cols_to_search: list, adj_iqr: float) -> DataFrame:

  """
  Imputes outliers in the DataFrame using the Interquartile Range (IQR) method.

  Parameters:
  input_df (DataFrame): The input DataFrame with data.
  cols_to_search (list): List of column names to search for outliers.
  adj_iqr (float): Adjustment multiplier for the IQR.

  Returns:
  DataFrame: The DataFrame with outliers imputed.
  """

  try:
    # Calculate Q1 and Q3
    quantiles = input_df.approxQuantile(cols_to_search, [0.25, 0.75], 0)
    medians = input_df.approxQuantile(cols_to_search, [0.5], 0)
    updated_df = input_df

    for i, col_name in enumerate(cols_to_search):
      median_value = medians[i][0]
      q1, q3 = quantiles[i]
      iqr = q3 - q1

      lower_bound = q1 - adj_iqr * iqr
      upper_bound = q3 + adj_iqr * iqr

      # print("col_name =", col_name)
      updated_df = updated_df.withColumn(
          col_name,
          when(
              ((col(col_name) < lower_bound) | (col(col_name) > upper_bound)), median_value
          ).otherwise(col(col_name))
      )
    return updated_df

  except Exception as e:
      # Log, cleanup, or perform other error handling here if necessary
      print(f"An error occurred: {e}")
      # Reraise the exception to pass it to the calling module
      raise


def split_df_by_missing_values(input_df: DataFrame, cols_to_search: list) -> (DataFrame, DataFrame):
  """
  Splits the input DataFrame into two DataFrames, one with missing values and one without.

  Parameters:
  input_df (DataFrame): The input DataFrame.
  cols_to_search (list): List of column names to check for missing values.

  Returns:
  (DataFrame, DataFrame): A tuple of two DataFrames, the first without missing values and the second with missing values.
  """
  try:
    condition = None
    for c in cols_to_search:
      temp = col(c).isNull()

      if condition is None:
        condition = temp
      else:
        condition = condition | temp

    newdf = input_df.filter(~condition)
    remdf = input_df.filter(condition)

    return newdf, remdf

  except Exception as e:
    print(f"An error occurred: {e}")
    raise


def filter_df_by_date(input_df: DataFrame, start: str, end: str) -> DataFrame:
  """
  Filters the input DataFrame for entries between the start and end dates.

  Parameters:
  input_df (DataFrame): The input DataFrame.
  start (str): Start date/time as a string.
  end (str): End date/time as a string.

  Returns:
  DataFrame: The filtered DataFrame.
  """
  try:
    # Convert start and end strings to timestamps
    start_time = to_timestamp(lit(start), "dd/MM/yyyy HH:mm:ss")
    end_time = to_timestamp(lit(end), "dd/MM/yyyy HH:mm:ss")
    
    # Filter the DataFrame for the specified date range
    return input_df.filter((input_df.timestamp >= start_time) & (input_df.timestamp <= end_time))

  except Exception as e:
    print(f"An error occurred: {e}")
    raise


def summary_statistics(input_df: DataFrame, group_key: str, target: str, start: str, end: str) -> DataFrame:
  """
  Calculates summary statistics (min, max, mean, stddev) for the specified target column, grouped by the group_key, for a given time period.

  Parameters:
  input_df (DataFrame): The input DataFrame.
  group_key (str): The column name to group by.
  target (str): The target column for which to calculate statistics.
  start (str): Start date/time as a string.
  end (str): End date/time as a string.

  Returns:
  DataFrame: A DataFrame containing summary statistics.
  """
  try:
    # Filter the DataFrame for the specified date range
    period_df = filter_df_by_date(input_df, start, end)

    # Calculate summary statistics for the specified target column, grouped by group_key
    stats_df = period_df.groupBy(group_key).agg(
      min(target).alias('min_' + target),
      max(target).alias('max_' + target),
      mean(target).alias('mean_' + target),
      stddev(target).alias('stdv_' + target)
    )

    stats_df=stats_df.\
    withColumn("from_date",to_timestamp(lit(start),"dd/MM/yyyy HH:mm:ss")).\
    withColumn("to_date",to_timestamp(lit(end),"dd/MM/yyyy HH:mm:ss"))
    
    return stats_df

  except Exception as e:
    print(f"An error occurred: {e}")
    raise


def find_outliers(input_df: DataFrame, stats_df: DataFrame, join_key, target, mean_col, stdv_col, start, end) -> DataFrame:
  """
  Identifies outliers in the input DataFrame based on statistical thresholds (mean ± 2*stddev) provided in stats_df.

  Parameters:
  input_df (DataFrame): The input DataFrame.
  stats_df (DataFrame): A DataFrame containing mean and standard deviation statistics for comparison.
  join_key (str): The key column to join input_df and stats_df.
  target (str): The target column in which to identify outliers.
  mean_col (str): The column name representing the mean value in stats_df.
  stdv_col (str): The column name representing the standard deviation in stats_df.
  start (str): Start date/time as a string for filtering data.
  end (str): End date/time as a string for filtering data.

  Returns:
  DataFrame: A DataFrame containing the identified outliers.
  """

  try:
    # Define lower and upper bounds for outlier detection
    lower = (col(mean_col) - (2 * col(stdv_col)))
    upper = (col(mean_col) + (2 * col(stdv_col)))

    # Filter the input DataFrame for the specified date range
    filtered_df = filter_df_by_date(input_df, start, end)

    # Join the input DataFrame with the stats DataFrame and filter out the outliers
    outliers_df = filtered_df.join(stats_df, join_key, 'inner').filter(
      (col(target) < lower) | (col(target) > upper)
    )

    return outliers_df

  except Exception as e:
    print(f"An error occurred: {e}")
    raise

def write_to_database(input_df:DataFrame,url:str,target_table:str,mode:str,user:str,password:str):
  """
  Writes the input DataFrame to a PostgreSQL database table specified by the target_table parameter.

  Parameters:
  input_df (DataFrame): The DataFrame to write to the database.
  url (str): Connection URL for the PostgreSQL database.
  target_table (str): The target table name where data will be written.
  mode (str): Specifies the behavior of the save operation when data already exists.
  user (str): Username for database authentication.
  password (str): Password for database authentication.

  """
  try:
    input_df.write.format("jdbc")\
    .option("url", url) \
    .option("driver", "org.postgresql.Driver").option("dbtable", target_table) \
    .option("user", user).option("password", password).mode(mode).save()

  except Exception as e:
    print(f"An error occurred: {e}")
    raise

