# Wind Turbine Data Processing Pipeline

## Description

This project is a data processing pipeline for a renewable energy company operating a farm of wind turbines. It ingests raw data from the turbines, cleans it, calculates summary statistics, identifies anomalies, and stores the processed data for further analysis. The pipeline is designed with a focus on scalability and testability, using Python and PySpark.

## Features

- **Data Cleaning**: Removes or imputes missing values and outliers from the raw turbine data.
- **Summary Statistics**: Calculates minimum, maximum, and average power output for each turbine over a 24-hour period.
- **Anomaly Detection**: Identifies turbines with power output significantly deviating from the expected range (outside of 2 standard deviations from the mean).
- **Data Storage**: Stores cleaned data and summary statistics in a database for further analysis.

## Assumptions

**Outlier Detection and Imputation Method**:
The solution uses the Interquartile Range (IQR) method for outlier detection and imputation, assuming it is appropriate for the data characteristics. It assumes that replacing outliers with median values is an acceptable strategy for the given context; Including using an adjusted IQR factor of 1.05

**Handling Missing Values**:
The solution presupposes that records extracted during data cleansing are preserved in a distinct DataFrame for subsequent processing by the main module.

## Installation

Ensure you have Python and PySpark installed on your system. Then, install the required dependencies:

```bash
pip install -r requirements.txt
```

## Pre-requisites

Ensure you have a postgres server running and create the necessary schemas,tables by excuting the following  (update the below with the actual details of your postgres DB):

```bash
PGPASSWORD="Password123!" psql -h localhost -p 5432 -d colibri -U colibri -f create_wind_turbine_tables.sql
```

Alternatively, if you have docker installed you can run the following script to create a docker container with a postgres running instance
the docker configuration is stored in docker-compose.yaml. Feel free to edit to accomodate your needs

```bash
./create_db.sh
```

## Usage


To run the data processing pipeline, you need to execute the following command:

```bash
cd colibri_code_challenge
python colibri_code_challenge.py
```

## Data

Data is provided as daily appended CSV files, each containing data for a group of 5 turbines. The naming convention of the files is `data_group_<group_number>.csv`.

## Testing

Run the test suite to ensure the pipeline's functionality:

```bash
cd test
pytest test_process_wind_turbine_data.py
```

## Contributing


## License



## Acknowledgements


