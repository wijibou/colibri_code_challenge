#!/usr/bin/bash

# Start up Docker containers in detached mode
docker-compose up -d

# Wait for PostgreSQL to be ready
echo "Waiting for PostgreSQL to start"
while ! PGPASSWORD="Password123!" psql -h localhost -p 5432 -d colibri -U colibri -c '\q'; do
  >&2 echo "PostgreSQL is unavailable - sleeping"
  sleep 1
done

>&2 echo "PostgreSQL is up - executing command"
PGPASSWORD="Password123!" psql -h localhost -p 5432 -d colibri -U colibri -f create_wind_turbine_tables.sql

