import pytest

from pyspark.sql import SparkSession, DataFrame, Row
from pyspark.sql.types import StructType, StructField, StringType, IntegerType, DoubleType, TimestampType

from pyspark.sql.functions import (
    col, percentile_approx, to_timestamp, trim, lit, 
    min, max, mean, stddev, count, isnan, isnull, when
)

from colibri_code_challenge.process_wind_turbine_data import (
create_spark_session, read_data, imputeOutliers, split_df_by_missing_values, filter_df_by_date, summary_statistics, find_outliers,write_to_database
)



# Setup a SparkSession for testing purposes
@pytest.fixture(scope="module")
def spark():
  return SparkSession.builder \
      .appName("Testing") \
      .getOrCreate()

# Test cases

def test_create_spark_session(spark):
  """
  Test the creation of a Spark session.
  """
  assert spark is not None, "Failed to create a Spark Session"


def test_read_data(spark):
  """
  Test reading data into a DataFrame.
  """

  sample_file_path = "data/data_group_sample_1.csv"
  schema = "timestamp STRING, turbine_id INT, wind_speed DOUBLE, wind_direction INT, power_output DOUBLE"
  df = read_data(spark, sample_file_path, schema)
  
  assert df is not None and isinstance(df, DataFrame), "Failed to read data into DataFrame"


def test_imputeOutliers(spark):
  """
  Test the imputeOutliers function to ensure it correctly imputes outlier values.
  """
  # Sample data to mimic a small dataset with potential outliers
  data = [
      Row(col1=1, col2=1),
      Row(col1=2, col2=7),  # outlier
      Row(col1=3, col2=9),
      Row(col1=4, col2=100),
      Row(col1=5, col2=200)   # outlier
  ]
  df = spark.createDataFrame(data)

  # Assume col2 has potential outliers
  cols_to_search = ["col2"]
  adj_iqr = 1.05  # typical IQR adjustment value

  # print("df before impute")
  # df.show()

  # Call the function under test
  result_df = imputeOutliers(df, cols_to_search, adj_iqr)
  # print("df after impute")
  # result_df.show()

  # Check the result to ensure it no longer contains the original outliers
  result = result_df.filter(col("col2") > 100).collect()
  assert len(result) == 0, "Outliers were not properly imputed"


def test_split_df_by_missing_values(spark):
  """
  Test the split_df_by_missing_values function to ensure it correctly 
  splits the input DataFrame into two DataFrames, one with missing values and one without.
  """
  # Creating a sample DataFrame
  data = [(1, None), 
  (2, "01-03-2022 02:00:00"), 
  (3, "01-03-2022 02:00:00"), 
  (None,"01-03-2022 02:00:00") ]

  columns = ["turbine_id", "timestamp"]
  input_df = spark.createDataFrame(data, schema=columns)

  # Test the function
  non_missing_df, missing_df = split_df_by_missing_values(input_df, columns)

  assert non_missing_df.count() == 2
  assert missing_df.count() == 2


def test_filter_df_by_date(spark):
  """
  Test the filter_df_by_date Filters the input DataFrame for entries between the start and end dates.
  """
  # Creating a sample DataFrame
  data = [Row("01-03-2022 10:00:00"), Row("15-03-2022 12:00:00"), Row("20-03-2022 15:00:00")]

  columns = StructType([
        StructField("timestamp", StringType(), True)
    ])
  input_df = spark.createDataFrame(data, schema=columns)

  # Convert string to timestamp explicitly
  input_df = input_df.withColumn("timestamp", to_timestamp("timestamp", "dd-MM-yyyy HH:mm:ss"))
  
  # Test the function
  start = "01/03/2022 00:00:00"
  end = "16/03/2022 00:00:00"
  filtered_df = filter_df_by_date(input_df, start, end)
  input_df.show()
  assert filtered_df.count() == 2  # Should only include the first two rows

def test_summary_statistics(spark):
  """
  Test the summary_statistics function to ensure it correctly calculates and returns
  summary statistics (min, max, mean, stddev) for the specified target column,
  grouped by the group_key, for a given time period.
  """
  # Define schema for the input data
  schema = StructType([
      StructField("turbine_id", IntegerType(), True),
      StructField("timestamp", StringType(), True),
      StructField("power_output", IntegerType(), True)
  ])

  # Create sample data for testing
  data = [
      Row(1, "01-03-2022 10:00:00", 100),
      Row(1, "15-03-2022 12:00:00", 200),
      Row(2, "15-03-2022 12:00:00", 300),
      Row(2, "20-03-2022 15:00:00", 400)
  ]

  expected_min_values = {1: 100, 2: 300}

  # Convert timestamp strings to actual timestamp data type
  input_df = spark.createDataFrame(data, schema=schema)
  input_df = input_df.withColumn("timestamp", to_timestamp("timestamp", "dd/MM/yyyy HH:mm:ss"))

  # Define parameters for the function
  group_key = "turbine_id"
  target = "power_output"
  start_date = "01/03/2022 00:00:00"
  end_date = "21/03/2022 00:00:00"

  # Run the summary_statistics function
  result_df = summary_statistics(input_df, group_key, target, start_date, end_date)
  

  # Collect the results to a list for easy comparison
  results_list = result_df.collect()

  # Iterate through the results and assert the min values
  for row in results_list:
      assert row["min_power_output"] == expected_min_values[row["turbine_id"]], f"Min value for turbine_id {row['turbine_id']} does not match expected value."


from pyspark.sql.functions import to_timestamp

def test_find_outliers(spark):
  """
  Test the find_outliers function to ensure it correctly identifies outliers
  based on the provided statistical thresholds (mean ± 2*stddev).
  """
  # Create sample data for testing 
  schema = StructType([
      StructField("turbine_id", IntegerType(), True),
      StructField("timestamp", StringType(), True),  # Temporarily set as StringType
      StructField("power_output", DoubleType(), True)
  ])

  data = [
      (1, "01-03-2022 10:00:00", 100.0),
      (1, "15-03-2022 12:00:00", 150.0),
      (1, "20-03-2022 15:00:00", 300.0),  # outlier
  ]

  input_df = spark.createDataFrame(data, schema=schema)
  input_df = input_df.withColumn("timestamp", to_timestamp("timestamp", "dd-MM-yyyy HH:mm:ss"))

  # Define statistics DataFrame (stats_df)
  stats_data = [(1, 125.0, 50.0)]  # mean 125.0, stddev 50.0 for turbine_id 1
  stats_schema = StructType([
      StructField("turbine_id", IntegerType(), True),
      StructField("mean_power_output", DoubleType(), True),
      StructField("stdv_power_output", DoubleType(), True)
  ])
  stats_df = spark.createDataFrame(stats_data, schema=stats_schema)

  # Define parameters for the function
  join_key = "turbine_id"
  target = "power_output"
  mean_col = "mean_power_output"
  stdv_col = "stdv_power_output"
  start_date = "01/03/2022 00:00:00"
  end_date = "21/03/2022 00:00:00"

  outliers_df = find_outliers(input_df, stats_df, join_key, target, mean_col, stdv_col, start_date, end_date)

  # outliers_df.show()

  # Verify the results by checking that the outlier is identified correctly
  assert outliers_df.count() == 1, "Outliers count does not match expected value."
  assert outliers_df.first()["power_output"] == 300.0, "Outlier value does not match expected value."


# Run the tests
if __name__ == "__main__":
  pytest.main()
